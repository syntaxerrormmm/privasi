# Istruzioni per il mantenimento del sito

## Test del sito

### Installazione di Hugo

Il sito è generato grazie a [Hugo](https://gohugo.io/), un popolare generatore statico di siti che consente di creare i propri siti con facilità mediante il linguaggio Markdown, garantendo al contempo flessibilità nella gestione dei contenuti se si intende modificare la struttura HTML e CSS delle pagine.

Per installare Hugo sulla propria distribuzione **Linux** è sufficiente ottenere il programma dal proprio gestore di pacchetti, mentre per altri sistemi operativi quali **Windows** o **macOS** è necessario leggere la [relativa pagina contenente le istruzioni](https://gohugo.io/getting-started/installing/). Per le principali distribuzioni Linux:

* Ubuntu:
```bash
        sudo apt install hugo
```
* Fedora:
```bash
        sudo dnf install hugo
```
* Arch Linux:
```bash
        sudo pacman -S hugo
```


### Clonazione repo PrivaSì

Per clonare la repo assieme a tutti i sottomoduli necessari al sito, utilizzate il comando

```bash
git clone --recursive https://gitlab.com/etica-digitale/privasi.git
```

Se avete già una copia locale aggiornata della repo ma senza sottomoduli, invece scaricateli tramite:

```
git submodule init
git submodule update
```

L'unico sottomodulo attuale è `themes/indigo`, nonché il tema [indigo](https://github.com/AngeloStavrow/indigo) utilizzato dal sito.

### Server in locale del sito per testare cambiamenti

Recatevi nella cartella radice della repo `privasi`, ed eseguite il comando 
```bash
hugo server -D
```
il quale costruirà il sito in locale all'indirizzo `https://localhost:1313/` (Ctrl+C per terminarlo).  

#### /dev, la sezione nascosta per testare

Si tenga conto che i capitoli non ancora rilasciati NON appaiono nel sito di base: per accedere alla lista di tutti i capitoli, aggiungete `/dev` all'indirizzo principale (`https://localhost:1313/dev`). Vi ritroverete così con una lista di tutti i file presenti in `content/percorso`, per poter testare tutte le varie modifiche.

## Generazione sito

Il sito impiega [GitLab Pages](https://gitlab.com/pages) per l'hosting dei contenuti. Si tratta di un sistema che consente di sfruttare le [GitLab CI/CD](https://docs.gitlab.com/ee/ci/) (*Continuous Integration, Continuous Delivery*) per generare automaticamente il proprio sito a partire da una repo GitLab.  
Per fare ciò, è stato creato un file `.gitlab-ci.yml`, il quale viene automaticamente identificato da GitLab e consente di avviare la generazione del sito web ogni qualvolta si effettua un `push` verso il ramo del sito. Questo avviene nella cartella `public` (ignorata dal `.gitignore`), contenente il sito vero e proprio.

## Il file 'config.yaml'

La configurazione principale del sito avviene nel file `config.yaml`, oppure `config.toml`. Hugo supporta ambedue i formati di configurazione (del tutto equivalenti ai fini della progettazione del sito). Per il progetto è stato scelto il formato YAML. Online è frequente trovare documentazioni, suggerimenti e pezzi di configurazione *sia in YAML che in TOML*, ma essendo i due equivalenti, se non si conoscono i formati è sufficiente usare un convertitore automatico (basta fare una ricerca su DuckDuckGo).

Le informazioni contenute nel file saranno spiegate nelle sezioni immediatamente successive, e per una descrizione maggiormente dettagliata si rimanda alla [documentazione ufficiale](https://gohugo.io/getting-started/configuration/).

### Principali

`baseUrl: "https://privasi.eticadigitale.org/"`
Contiene l'URL di base del sito, ovvero l'URL della pagina home. 

`canonifyURLs: false`
Disabilita gli URL relativi, e rende ogni URL assoluto a partire dalla radice.

`contentDir: "content"`
La cartella contenente il sito, dalla quale Hugo preleva i contenuti delle pagine in Markdown.

`layoutDir: "layouts"`
La cartella contenente le modifiche all'HTML.

`publishDir: "public"`
La cartella contenente il sito pubblicato, una volta generato con `hugo server -D`. Si rammenta che questa cartella è ignorata da `git` in quanto indicata nel file `.gitignore`.

`theme: "indigo"`
Il nome della cartella del tema adoperato da Hugo. I temi devono essere contenuti nella cartella `themes` salvo diverse indicazioni.

`enableEmoji: true`
Abilita le emoji.

`comments: false`
Disabilita i commenti.

`javascript: false`
Disabilita JavaScript.

`metaDataFormat: yaml`
Impone che il formato automatico per il Front Matter delle pagine sia YAML.

```
outputFormats:
        html:
                isPlainText: true
```
Rende possibile usare l'HTML nella sezione `Title:` del Front Matter delle pagine.

### `params:`
Nella sezione `params:` sono contenuti:

`author: Etica Digitale`
Autore del sito.

`description: Percorso di Etica Digitale`
Descrizione del sito.

`avatar: "images/ed_logo.svg"`
Percorso del logo del sito.

### `menu:` `main:`
La sezione contiene i collegamenti nell'header. Ciascun collegamento è composto da:

`name:` nome del collegamento;

`url:` percorso del collegamento;

`weight:` numero indicante il "peso" dell'elemento, l'ordine con cui compare.

---

## Implementazione di un capitolo nel sito

Gli articoli del sito vanno collocati nella cartella `content/percorso/`, a partire dalla radice della repo. La cartella `content` include anche altri file importanti come `indice.md` e `FAQ.md` contenenti, rispettivamente, l'indice del Percorso e le domande più frequenti; sono inoltre collocate, nella cartella `images`, anche le immagini `gif`, `png` e `jpg` necessarie ai vari capitoli.

### Titolo
Per il titolo si usa il [Front Matter](https://gohugo.io/content-management/front-matter/) di Hugo, indicando semplicemente il campo `Title` (i trattini sono obbligatori):

```markdown
---
Title: "Titolo del capitolo del Percorso"
---
```

Nei titoli in cui va introdotta formattazione particolare si usa l'HTML come da esempio (dove viene barrato "Messenger"):
```markdown
---
Title: "Conversazioni di tutti i giorni: <strike>Messenger</strike> Telegram"
---
```

### Immagini

Le immagini presenti nel Percorso sono così implementate: `![nome_del_file](../../images/nome_del_file)`. Per esempio:

```markdown
![L2-2_gif0.gif](../../images/L2-2_gif0.gif)
```
Sebbene la parte rilevante sia quella tra parentesi tonde, il vantaggio di indicare esattamente il nome del file nelle quadre è quello di riconoscere immediatamente l'immagine nel caso in cui non sia stata correttamente visualizzata, poiché al posto di essa ne comparirà il nome. È importante prestare attenzione alle lettere maiuscole e minuscole, altrimenti le immagini non verranno individuate da Hugo.

### Link e collegamenti ad altri capitoli del Percorso

I collegamenti agli altri capitoli potranno essere realizzati in due maniere differenti:
```
[Livello 1: "Rimozione Consenso"](../../percorso/l1-0___activity-deletion/)  
```

oppure

```
[Livello 1: "Rimozione Consenso"](/percorso/l1-0___activity-deletion/)
```

Si osserva che in Hugo, salvo indicato diversamente, **non** bisogna mettere nei link l'estensione del file ed è obbligatorio che sia tutto a lettere minuscole (nell'esempio sopra anziché avere `/percorso/L1-0___Activity-Deletion.md` avremo `/percorso/l1-0___activity-deletion/`). È importante prestare attenzione a questa regola, altrimenti i link non funzioneranno.

---

## Hugo in dettaglio e modifiche al tema del sito

### La cartella `layouts`

Nella cartella del tema, `themes/indigo`, è presente la cartella `layouts` contenente l'impaginazione delle pagine. Per modificarne il contenuto **non** si agisce sul tema, bensì si copiano e si incollano i file che si intende modificare nella cartella `layouts` fuori dal tema e nella repo. Hugo sceglierà prima i file contenuti all'interno della cartella nella nostra repo, ed ignorerà i corrispondenti file del tema se questi avranno lo stesso nome. 

Ad esempio, in `themes/indigo/layouts` è presente il file `index.html` che supponiamo di voler modificare. Non possiamo agire sul tema, prima di tutto perché si tratta di un sottomodulo esterno e poi perché esso viene ottenuto da GitLab separatamente (e quindi non potremmo comunque modificare i files del tema sul sito vero e proprio). Per modificare, copiamo quel files dentro `layouts` nel nostro repository, e lo modifichiamo: quel file verrà letto da Hugo, e ignorerà il file nella cartella del tema. Allo stesso modo, volendo modificare `themes/indigo/layouts/partials/footer.html` si crea dentro `layouts` nel nostro repository la cartella `partials`, e ci copiamo all'interno `footer.html`. Ancora una volta, Hugo sceglierà questo file prima di quello corrispondente nel tema, e dunque possiamo tranquillamente modificarlo lì.

Per modificare l'`index.html` ed il `footer.html` li abbiamo copiati, e collocati ciascuno nelle proprie posizioni. L'albero della cartella `layouts` è il seguente:

```
layouts
├── index.html
└── partials
   └── footer.html
```

#### `index.html`
Il file `index.html` è stato completamente spogliato della parte in cui mostra gli articoli in ordine di data (tipicamente contenuti in una cartella chiamata `posts` o `post`, ma che non sono necessari al Percorso) e contiene esclusivamente l'header, l'HTML della pagina iniziale, ed il footer (modificato anch'esso).

#### `footer.html`
Nel `footer.html` è stata rimossa la parte relativa ai bottoni che consentivano di proseguire alla pagina successiva o alla pagina precedente in ordine di data. La scelta è stata fatta per semplicità, e per consentire di non preoccuparsi della data indicata nel Front Matter dei singoli capitoli.

###  La cartella `static`
Nella cartella `static` sono contenute le favicon necessarie, il logo del sito, e una cartella `images` contente i loghi di Etica Digitale. Eventualmente, e sempre copiando quanto esiste in `themes/indigo/static` è possibile modificare il CSS del sito modificando gli opportuni files in questa cartella; è anche possibile aggiungere fonts ed icone. La favicon viene automaticamente scelta da Hugo (ignorando la favicon predefinita del tema) a patto che essa si trovi nella cartella `static`.

---

## Cambiamento di tema
Cambiare tema in Hugo è un processo potenzialmente lungo e complesso. Oltre ad aggiungere i sottomoduli necessari, ciascun tema ha un proprio comportamento, una propria struttura, e dei propri layout. Dunque, non è garantito che il layout del sito corrente funzioni senza modifiche una volta passati ad un tema diverso. Può anche darsi che qualche tema sia dipendente da JavaScript, abbia dipendenze da CDN esterne, o non goda di una licenza compatibile con la GPLv3. Pertanto, è importante fare attenzione a tutto ciò se si intende cambiare il tema del sito.

L'elenco dei temi di Hugo è liberamente disponibile [sul loro sito](https://themes.gohugo.io/).


---
