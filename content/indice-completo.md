---
Title: Indice
---

> **Come usare la lista**: Chi va piano, va sano e va lontano. L'obiettivo non è arrivare primi (non c'è nessun premio), bensì capire *cosa* fa il singolo punto e il *perché* è stato suggerito. Cliccateci su e si apriranno le istruzioni precedute da una breve spiegazione. Prendetevi il tempo che vi serve.

> **Come NON usare la lista**: Seguire ogni punto senza capirne il motivo. Chiudercisi in maniera compulsiva finché non si ha finito. Saltare tra i livelli.

*Attenzione: la lista non è ultimata e chiunque è libero di contribuire come spiegato [QUI](https://gitlab.com/etica-digitale/privasi/blob/master/FAQ.md#come-posso-contribuire-alla-documentazione). I lavori procedono regolarmente, e [QUI](https://gitlab.com/etica-digitale/privasi/commits/master) trovate il registro di tutte le modifiche.*
 
---

## Livello 0: Introduzione
* [Introduzione: cosa me ne importa della privacy?](../percorso/l0-0___intro/)
* [Formiche in una teca](../percorso/l0-1___ants/)
---
## Livello 1: Limitare i danni
* [Rimozione consenso](../percorso/l1-0___activity-deletion/)
* [Password 101](../percorso/l1-1___passwords/)
* [Conversazioni di tutti i giorni: ~~Messenger~~ Telegram](../percorso/l1-2___telegram/)
* [Conversazioni sensibili pt.1: Smart TV e assistenti vocali (Internet delle Cose)](../percorso/l1-3___iot/)
* [Conversazioni sensibili pt.2: Signal incontra WhatsApp](../percorso/l1-4___signal/)
* [Motori di ricerca: al di là di Google](../percorso/l1-5___search-engine/)
* [Libre Office](../percorso/l1-6___libre-office/)
* [Gesti Quotidiani](../percorso/l1-7___daily-habits/)
* [Fuga dal superfluo](../percorso/l1-8___accounts-deletion/)

---
## Livello 2: Mezzi per muoversi
* Cookie: briciole di internet
* Non nel mio nome pt.1: F-Droid, lo store trasparente
* Non nel mio nome pt.2: ~~Chrome~~ Bromite e Firefox, finestre della rete
* Mail temporanee
* ~~YouTube~~ Video in un riflesso
* ~~Google Maps~~ OpenStreetMap
* WebApps: social a compartimenti stagni
* Esodo

## Livello 3: Compartimentare
* Compartiche?
* Social e Professionale: profili Firefox
* Personale: Tor, cipolle digitali
* uBlock Origin
* Mail separate

## Livello 4: Pilastri del quotidiano

- Dove giace il confine
- ~~Spotify~~
- ~~WhatsApp~~
- ~~Discord~~
- Social: tra il fediverso e il nulla
- Streaming on-demand: dimmi cosa guardi e ti dirò chi sei
- Alternative etiche: una lista

## Livello 5: Chi fa da sé

- Freemium e nuovi modelli di business
- DNS
- ROM telefono
- GNU/Linux: il sistema operativo libero

