---
Title: "<strike>Google Maps</strike> OpenStreetMap"
---

> Nonostante Google Maps sia l'app di navigazione per eccellenza -in quanto installata di base su qualsiasi cellulare Android-, al di fuori di essa c'è un mondo creato da altre persone. Letteralmente!

## In parole semplici

Chiunque conosce Wikipedia: un'enorme enciclopedia libera. Libera perché accessibile da tutti, senza costo alcuno, dove chiunque è libero di contribuire. Parliamo del decimo sito più visitato al mondo[^1], ed è l'unico tra questi ad essere una fondazione (```.org```. E non un'attività commerciale, ```.com```). Ora immaginate Wikipedia, ma per le mappe di questo mondo: ecco, quello è OpenStreetMap.  

OpenStreetMap, o OSM per abbreviare, è nato nel 2004, pressapoco insieme a quello che l'anno dopo sarebbe diventato Google Maps (che non è stato creato da Google, bensì comprato. Insieme a sistemi satellitari diventati poi Google Earth e quant'altro). A differenza del coetaneo Google però, OpenStreetMap non era (e non è) privatizzato, rendendo l'attività di creare e consultare mappe un diritto di tutti. Chiunque può infatti connettersi al portale ufficiale https://www.openstreetmap.org/ e nel giro di pochi secondi mettersi ad aggiungere edifici, strade o persino cose più peculiari come idranti o panchine.  

Un'altra differenza sostanziale tra i due è che OpenStreetMap è un database, non un'app. Questo è importante perché vuol dire che le mappe di OSM possono essere utilizzate come base per creare infinite app, da quelle classiche a quelle per usi più specifici: da ciclisti, escursionisti, nautiche e tante altre. Vuole anche dire che qualsiasi servizio che usa le mappe di Google può essere sostituito da un servizio che usa quelle di OpenStreetMap; anzi, vi sarà sicuramente capitato di usare quelle di OSM e non saperlo neanche. Un esempio? Il gioco per telefono Pokémon Go[^2].

Come abbiamo imparato già dai primi capitoli, poi, Google Maps (o più in generale Google) è solito tracciare i nostri spostamenti: ma perché esattamente una mappa dovrebbe raccogliere informazioni su di noi ovunque andiamo (o dove restiamo)? Informazioni che, in alcuni casi, vengono condivise con la polizia causa sospetti[^3].  

Se si è stanchi di avere una sorta di stalker digitale, vediamo qui sotto con cosa è possibile sostituire Google Maps; sia desktop che mobile.

## Cosa fare

### Desktop

##### OpenStreetMap

Per Desktop troviamo il portale ufficiale di OpenStreetMap (https://www.openstreetmap.org/). Oltre che consultare una normale mappa, possiamo vederne una versione di mappe ciclabili, una di mezzi di trasporto e un'ultima di servizi generali (fontanelle pubbliche, negozi ecc.) semplicemente dal menù a destra "livelli"  
![L2-5_gif0.gif](../../images/L2-5_gif0.gif)

##### Qwant Maps

Qwant Maps è il servizio di mappe di Qwant[^4], che fonda la sua forza sulla tutela dei dati personali degli utenti. Grazie alla tecnologia di OpenStreetMap, permette di navigare in modo sicuro e anonimo in qualsiasi parte del mondo.
![L2-5_gif1.gif](../../images/L2-5_gif1.gif)
### Mobile

Qui dobbiamo differenziare tra le app su F-Droid e un'eccezione alla regola, che è possibile trovare solo sugli store normali. Su F-Droid ci sono essenzialmente due opzioni, OSMAnd+ e Maps, mentre l'eccezione di cui stiamo parlando è Magic Earth. Vediamo perché.


##### OSMAnd+
Passando a F-Droid, l'interfaccia di OSMAnd+ è molto da smanettoni e con una gamma cromatica non proprio discreta. Purtroppo non dispone di quella comodità d'uso che ci si potrebbe aspettare da un'app di navigazione, rendendola macchinosa ai più. Anche qui si possono scaricare le mappe suddivise per regione, se le si vuole usare offline. Un'app un po' per "pochi".

##### Organic Maps
Organic Maps è un'applicazione di mappe libere, senza traccianti, che visualizza le informazioni geografiche in modo rispettoso della privacy degli utenti. Queste mappe sono create utilizzando dati provenienti da OpenStreetMap e sono progettate per essere utilizzabili offline. E' disponibile negli store classici ma anche su F-Droid. Viene usata principalmente come navigatore con una vista in 3D e la possibilità di ricercare dei punti di interesse per tipologia (*Dove mangiare*, *Parcheggio*, *Bar*, ecc). Consigliata per nuovi utenti per la sua semplicità d'uso.

##### Magic Earth
Comparata alle altre due, non c'è storia: le mappe sono pulite, la navigazione è semplice, ha un'interessante modalità in 3D dove potete vedere altezze come quelle delle montagne, e inoltre supporta le mappe offline. Questo vuol dire che potete benissimo scaricare la mappa di una regione e usarla ovunque vogliate anche senza internet acceso.  
La cosa che infastidisce è che il codice non è libero, ma facendo un'analisi dei traccianti questi risultano nulli[^5]. Finché quindi l'app terrà questa linea, ci sentiamo di consigliarla.

Infine, se vi piace l'idea di creare mappe e volete fare un gesto del quale potranno beneficiarne tutti (voi inclusi), potreste provare a mappare anche voi :)  
C'è per esempio [LearnOSM](https://learnosm.org/it/), un sito apposta (anche in italiano) che spiega passo passo come funziona OSM e come mappare. O per un'infarinatura generale, [il libretto d'introduzione sulla wiki ufficiale](https://wiki.openstreetmap.org/wiki/File:Libretto_introduzione_a_OpenStreetMap_2017_09.pdf)!
[Torna al percorso](../../indice)  


## Appendice
[^1]: nel momento in cui ciò sta venendo scritto. La lista è consultabile su https://www.alexa.com/topsites  
[^2]: https://wiki.openstreetmap.org/wiki/Pok%C3%A9mon_Go  
[^3]: Congiu Gabriele, [Google: crescono le richieste di accesso ai dati GPS degli utenti da parte della polizia](https://www.hdblog.it/2019/04/15/google-richiesta-dati-posizione-polizia/), HD Blog, 2019
[^4]: [motori di ricerca meno consigliati](/percorso/l1-5___search-engine/#motori-meno-consigliati)
[^5]: https://reports.exodus-privacy.eu.org/en/reports/com.generalmagic.magicearth/latest/
