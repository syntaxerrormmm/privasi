---
Title: "Conversazioni sensibili pt.2: Signal incontra WhatsApp"
---

> WhatsApp è l'app di messaggistica più usata al mondo, fungendo da vero e proprio social in certi stati. Quanto è privato? Ci sono alternative sulla piazza? E come è possibile anche solo immaginare che altre persone possano considerare altre opzioni rispetto a questo colosso di messaggistica?

## In parole semplici
Cosa rende una conversazione privata? Spesso c'è confusione tra cos'è anonimo e privato, quindi per capirlo meglio immaginiamo che ci siano due persone chiuse in una stanza a parlare.  
Se NON abbiamo visto chi sono ma sappiamo **cosa** si stanno dicendo, la conversazione è anonima. Al contrario, se NON sappiamo *cosa* stanno dicendo ma sappiamo *chi* c'è dentro la stanza, la conversazione è privata.  
Quindi, possiamo dire che una conversazione è privata quando solo gli interlocutori possono sentire/vedere cosa viene detto. Sotto questo punto di vista, sia WhatsApp che Signal sono private.

Mettiamo, ora, che prima di entrare nella stanza ci sia una telecamera per vedere chi entra, quando, come e con chi: analizzando la registrazione si possono quindi capire le abitudini, con chi si parla di più, quando si è soliti entrare ecc. facendosi un'idea della persona.  
In informatica, chiamiamo questi dati di contorno **metadati**. E proprio i metadati son quelli che WhatsApp da contratto[^1] passa a Facebook (perché appartiene a Facebook[^2]) e a terze parti, per una migliore profilazione: ultimo accesso, quanto state connessi, con chi chattate di più, numero di telefono, numeri in rubrica, connessione internet, posizione GPS ecc.. Anche se non sanno cosa venga scritto, quindi, grazie ai metadati possono capire molto di noi. In sintesi, quando WhatsApp si vanta sul suo sito ufficiale[^3] che "La tua privacy è la nostra priorità", ora sapete perché sono un mucchio di stronzate.  

WhatsApp afferma, poi, di usare "crittografia end-to-end" per proteggere le chat. In parole povere, è un modo per impedire a esterni, azienda inclusa, di poterle leggere. Tuttavia, se non abbiamo un telefono Apple, quando i nostri messaggi vengono salvati sul cloud (ovvero un *backup*, che in questo caso viene fatto su Google Drive), i messaggi *non* sono crittografati. Questo permette a chiunque abbia l'accesso a quel determinato Google Drive di poter leggere le chat in chiaro[^4]. E ironicamente, come vedremo in futuro, Google può accedere ai Drive di tutti i suoi utenti. Ergo Google può leggere le vostre chat.  

"Sì, ma tutti quelli che conosco usano WhatsApp".  

Eh: questo, a seconda delle persone, può essere più o meno problematico. C'è chi ha contatti di lavoro, chi tutta la cerchia di amici, ma in sintesi una buona via di mezzo è iniziare affiancando l'app di Signal a Whatsapp. Al contrario di un'app alla quale importa della vostra privacy quanto a un'industria delle armi importa della pace, Signal offre la stessa protezione, con l'aggiunta che: non colleziona metadati, è completamente libera, non manda backup a terze parti mettendo a nudo le vostre conversazioni, gli sticker sono più comodi, ha un'intera comunità di supporto alla quale potete prendere parte se masticate l'inglese[^5] e supporta messaggi che si autodistruggono.

## Cosa fare
Come Telegram, trovate Signal sullo store delle app. Installatela, e seguite semplicemente le istruzioni. Per capire chi della vostra rubrica ha già l'applicazione e chi no, date un occhio alla rubrica *dentro Signal*.  

Torniamo ora al discorso della gente su Whatsapp, che non è disposta a seguirvi. Perché deve essere chiaro che dei vostri conoscenti non lo farà quasi nessuno. E in fondo va bene così, perché tanto c'è una buona probabilità che non vi importi *davvero* dell'intera rubrica. Vi importa di amici, partner, forse genitori. Vi importa che *quelle* conversazioni tornino a essere vostre. Che le emozioni, dubbi, gioie, paure condivise via messaggio rimangano vostre, e non di un'azienda che trasformerà il contesto che le circonda in merce per fare soldi. Parlatene quindi a loro, dove il rapporto reciproco non farà pesare lo scaricare un'app per voi, mentre per quanto riguarda il parlarne agli altri... davvero, come preferite, ma non fasciatevici la testa.  

Arrivati alla fine di questo capitolo, avete ora un'applicazione per le conversazioni "pubbliche" (Telegram) e una per quelle più sensibili (Signal). Non c'è molto altro da aggiungere, se non che da ora in avanti starà a voi decidere quando usare una o l'altra. Fatene buon uso.  

[Torna al percorso](../../indice)  

## Appendice
[^1]: Contratto WhatsApp: https://www.whatsapp.com/legal/?eea=1&lang=it  
[^2]: Covert Adrian, [Facebook buys WhatsApp for $19 billion](https://money.cnn.com/2014/02/19/technology/social/facebook-whatsapp/index.html), CNN, 2014  
[^3]: https://www.whatsapp.com/privacy/?lang=it&_fb_noscript=1  
[^4]: Osborne Charlie, [WhatsApp warns free Google Drive backups are not end-to-end encrypted](https://www.zdnet.com/article/whatsapp-warns-free-google-drive-backups-are-not-encrypted/), ZDNet, 2018
[^5]: https://community.signalusers.org/  
